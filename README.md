`docker build -t videoshow .`

- All images must be the same size
- Input paths need to be absolute
- Cannot specify logo params using config or command-line, must implement API
- https://github.com/h2non/videoshow/blob/master/bin/videoshow
- https://gitlab.com/shareroot/development/videomaker-server/blob/master/Dockerfile
- https://sharp.pixelplumbing.com/en/stable/api-operation/
- http://ffmpeg.org

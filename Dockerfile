FROM node

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get -y install ffmpeg
RUN npm install -g videoshow

ENTRYPOINT ["/usr/local/bin/videoshow"]

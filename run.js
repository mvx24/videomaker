#!/usr/bin/env node

const path = require('path');
const childProcess = require('child_process');

if (process.argv[2]) {
  const input = path.resolve(process.argv[2]);
  const output = path.resolve(process.argv[3] || 'output');
  const cmd = `docker run -v ${input}:/run/videoshow -v ${output}:/run/output --rm videoshow -c /run/videoshow/config.json`;
  childProcess.execSync(cmd, { stdio: [0, 1, 2] });
} else {
  console.log('./run.js <input-dir> <output-dir>');
}
